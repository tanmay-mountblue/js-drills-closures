module.exports.limitFunctionCallCount = (cb, n) => {
   
   return function() {
       if(n>0){
        n--;
        return cb();
       }
       else{
           return console.log(null);
       }
   }
}