module.exports.counterFactory = () => {
    let counter = 0;
    function increament(){
        return ++counter;
    }
    function decreament(){
        return --counter;
    }

    return {
        increament,
        decreament
    }
}