module.exports.cacheFunction = (cb) => {
   
    let cache = {};
    return function(val) {
        if(cache.hasOwnProperty(val)){
            return cache[val];
        }else{
            let res=cb(val);
            cache[val]=res;
            return res;
        }
        
    }
 }